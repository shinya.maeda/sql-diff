WITH "shas" AS (
    SELECT
        "merge_request_diff_commits"."sha"
    FROM
        "merge_request_diff_commits"
    WHERE
        "merge_request_diff_commits"."merge_request_diff_id" IN (
            SELECT
                "merge_request_diffs"."id"
            FROM
                "merge_request_diffs"
            WHERE
                "merge_request_diffs"."merge_request_id" = 32
            ORDER BY
                "merge_request_diffs"."id" DESC
            LIMIT
                100
        )
    LIMIT
        10000
)
SELECT
    "ci_pipelines".*
FROM
    (
        (
            SELECT
                "ci_pipelines".*
            FROM
                "ci_pipelines"
                INNER JOIN "shas" ON "shas"."sha" = "ci_pipelines"."source_sha"
            WHERE
                (
                    "ci_pipelines"."config_source" IN (1, 2, 4, 5)
                    OR "ci_pipelines"."config_source" IS NULL
                )
                AND "ci_pipelines"."source" = 10
                AND "ci_pipelines"."merge_request_id" = 32
                AND "ci_pipelines"."project_id" IN (131, 130)
        )
        UNION
            (
                SELECT
                    "ci_pipelines".*
                FROM
                    "ci_pipelines"
                    INNER JOIN "shas" ON encode("shas"."sha", 'hex') = "ci_pipelines"."sha"
                WHERE
                    (
                        "ci_pipelines"."config_source" IN (1, 2, 4, 5)
                        OR "ci_pipelines"."config_source" IS NULL
                    )
                    AND "ci_pipelines"."source" = 10
                    AND "ci_pipelines"."merge_request_id" = 32
                    AND "ci_pipelines"."project_id" IN (131, 130)
            )
        UNION
            (
                SELECT
                    "ci_pipelines".*
                FROM
                    "ci_pipelines"
                    INNER JOIN "shas" ON encode("shas"."sha", 'hex') = "ci_pipelines"."sha"
                WHERE
                    "ci_pipelines"."project_id" = 131
                    AND (
                        "ci_pipelines"."config_source" IN (1, 2, 4, 5)
                        OR "ci_pipelines"."config_source" IS NULL
                    )
                    AND (
                        "ci_pipelines"."source" IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13)
                        OR "ci_pipelines"."source" IS NULL
                    )
                    AND "ci_pipelines"."ref" = 'feature-asda'
                    AND "ci_pipelines"."tag" = FALSE
            )
    ) ci_pipelines
ORDER BY
    CASE ci_pipelines.source WHEN (10) THEN 0 ELSE 1 END,
    ci_pipelines.id DESC
